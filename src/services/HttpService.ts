import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { UserModule } from '@/store';

const SERVER_URL = process.env.NODE_ENV === 'production'
  ? 'https://api.spiderbiggen.com/api-turnips/'
  : 'http://localhost:3000/';

class HttpService {
  private readonly instance: AxiosInstance;

  constructor () {
    this.instance = Axios.create({
      baseURL: SERVER_URL,
      headers: {
        'Content-Type': 'application/json',
      },
    });
    this.instance.interceptors.request.use(
      value => {
        value.headers.Authorization = `Bearer ${UserModule.token}`;
        return value;
      },
    );

    this.instance.interceptors.response.use(
      value => {
        if (value.data.status === 'success' && value.data.data) {
          value.data = value.data.data;
        }
        return value;
      },
      async error => {
        if (error.response.status === 401 && !/refresh/.test(error.config.url)) {
          if (await UserModule.refreshToken()) {
            return (await this.instance.request(error.config)).data;
          }
        }
        throw error;
      },
    );
  }

  async head<T = any> (url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.head<T>(url, config)).data;
  }

  async get<T = any> (url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.get<T>(url, config)).data;
  }

  async post<T = any> (url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.post<T>(url, data, config)).data;
  }

  async patch<T = any> (url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.patch<T>(url, data, config)).data;
  }

  async put<T = any> (url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.put<T>(url, data, config)).data;
  }

  async delete<T = any> (url: string, config?: AxiosRequestConfig): Promise<T> {
    return (await this.instance.delete<T>(url, config)).data;
  }
}

export default new HttpService();
