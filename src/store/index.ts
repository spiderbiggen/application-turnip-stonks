import Vue from 'vue';
import Vuex, { Plugin } from 'vuex';
import VuexPersist from 'vuex-persist';

import { getModule } from 'vuex-module-decorators';
import { AppStore } from '@/store/modules/AppStore';
import { UserStore } from '@/store/modules/UserStore';
import { TurnipStore } from '@/store/modules/TurnipStore';

Vue.use(Vuex);

function isStorageSupported (storage: Storage = localStorage) {
  const testKey = 'test';
  try {
    storage.setItem(testKey, '1');
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
}

const plugins: Plugin<any>[] = [];
if (isStorageSupported() || isStorageSupported(sessionStorage)) {
  const vuexLocalStorage = new VuexPersist({
    key: 'turnips-ls',
    storage: isStorageSupported() ? localStorage : sessionStorage,
    modules: ['user', 'app'],
  });
  const vuexSessionStorage = new VuexPersist({
    key: 'turnips-ss',
    storage: sessionStorage,
    modules: ['turnip'],
  });
  plugins.push(vuexLocalStorage.plugin, vuexSessionStorage.plugin);
}

const config = {
  modules: {
    app: AppStore,
    user: UserStore,
    turnip: TurnipStore,
  },
  plugins: plugins,
};
const store = new Vuex.Store(config);
export const AppModule = getModule(AppStore, store);
export const UserModule = getModule(UserStore, store);
export const TurnipModule = getModule(TurnipStore, store);

export default store;
