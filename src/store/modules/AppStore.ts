import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { IStore } from '@/store/modules/IStore';
import { TurnipModule, UserModule } from '@/store';

@Module({ name: 'app' })
export class AppStore extends VuexModule implements IStore {
  @Action({ rawError: true })
  async start () {
    if (await UserModule.refreshToken()) {
      await Promise.all([
        UserModule.getSelf(),
      ]);
    }
    await Promise.all([
      TurnipModule.init(),
    ]);
  }

  @Mutation
  clear (): void {
  }
}
