import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { IStore } from '@/store/modules/IStore';
import { Optional } from '@/models/Types';
import { socket } from '@/services/Websocket';
import { TurnipModule, UserModule } from '@/store';
import { FullPlayer, RoomUpdate } from '@/models/TurnipsResponseModels';

let connected = false;

@Module({ name: 'turnip' })
export class TurnipStore extends VuexModule implements IStore {
  status: Optional<RoomUpdate> = null;
  player: Optional<FullPlayer> = null;
  session: Optional<string> = null;

  @Action
  async init () {
    await TurnipModule.connect();
  }

  @Action({ rawError: true })
  async connect () {
    // if (socket.connected) return;
    socket.on('connect', () => {
      connected = true;
      // socket.emit('join', 'banaan');
      socket.on('update', (update: RoomUpdate) => {
        TurnipModule.setStatus(update);
      });
      socket.on('joined', TurnipModule.setPlayer);
    });
    socket.connect();
  }

  @Action({ rawError: true })
  async subscribe () {
    const player: Partial<FullPlayer> = Object.assign({}, TurnipModule.player || UserModule.player);
    const code = TurnipModule.session;
    if (!code || !socket.connected || !player) return;
    player.sessionId = code;
    socket.emit('queue', player);
  }

  @Action({ rawError: true })
  async host ({ dodo, first = false }: { dodo: string, first: boolean }) {
    const player: Partial<FullPlayer> = Object.assign({}, TurnipModule.player || UserModule.player);
    if (!socket.connected || !player) return;
    if (first && player?.id) delete player.id;
    socket.emit('host', { ...player, dodo: dodo });
  }

  @Mutation
  setStatus (status: Optional<RoomUpdate>) {
    this.status = status || null;
  }

  @Mutation
  setPlayer (player: Optional<FullPlayer>) {
    this.player = player || null;
  }

  @Mutation
  setSession (session: Optional<string>) {
    this.session = session || null;
  }

  @Mutation
  clear (): void {
    if (socket.connected) {
      socket.disconnect();
    }
    this.status = null;
  }
}
