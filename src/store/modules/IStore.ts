import { VuexModule } from 'vuex-module-decorators';
import { UserModule } from '@/store';

export abstract class IStore extends VuexModule {
  static clearAll () {
    UserModule.clear();
  }

  abstract clear (): void;
}
