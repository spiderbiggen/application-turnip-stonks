import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import HttpService from '@/services/HttpService';
import { IStore } from '@/store/modules/IStore';
import { UserModule } from '@/store';
import { TokenRequest } from '@/models/Requests';
import { Optional } from '@/models/Types';
import { User } from '@/models/User';
import { Player } from '@/models/TurnipsResponseModels';

@Module({ name: 'user' })
export class UserStore extends VuexModule implements IStore {
  token: Optional<string> = null;
  refresh: Optional<string> = null;
  self: Optional<User> = null;
  player: Optional<Partial<Player>> = null;

  get loggedIn () {
    return !!this.token;
  }

  @Action
  public async logout () {
    IStore.clearAll();
  }

  @Action({ rawError: true })
  public async authorize (payload: { email: string, password: string }) {
    try {
      const { token, refresh_token } = await HttpService.post<TokenRequest>('users/authenticate', payload);
      UserModule.setToken(token);
      if (refresh_token) {
        UserModule.setRefreshToken(refresh_token);
      }
    } catch (error) {
      if (error.response.status === 401) {
        IStore.clearAll();
      }
      throw error;
    }
  }

  @Action({ rawError: true })
  public async register (payload: { email: string, password: string, username: string }) {
    await HttpService.post<User>('users/register', payload);
    await UserModule.authorize(payload);
  }

  @Action({ rawError: true })
  public async getSelf () {
    try {
      const self = await HttpService.get<User>('users/me');
      UserModule.setSelf(self);
    } catch (e) {
      console.error(e);
    }
  }

  @Action({ rawError: true })
  public async refreshToken () {
    const refresh = UserModule.refresh;
    if (!refresh) return false;
    try {
      const { token, refresh_token } = await HttpService.post<TokenRequest>('users/refresh', { refresh: refresh });
      await UserModule.setToken(token);
      if (refresh_token) {
        await UserModule.setRefreshToken(refresh_token);
      }
      return true;
    } catch (error) {
      if (error.response.status === 401) {
        IStore.clearAll();
      }
    }
    return false;
  }

  @Mutation
  setToken (token: Optional<string>) {
    this.token = token;
  }

  @Mutation
  setPlayer (player: Optional<Partial<Player>>) {
    this.player = player;
  }

  @Mutation
  setRefreshToken (token: Optional<string>) {
    this.refresh = token;
  }

  @Mutation
  setSelf (self: Optional<User>) {
    this.self = self;
  }

  @Mutation
  clear (): void {
    this.token = null;
    this.refresh = null;
    this.self = null;
  }
}
