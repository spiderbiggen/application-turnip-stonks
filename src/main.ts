import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import App from './App.vue';
import moment from 'moment';
import '@/store';

import '@/assets/scss/base.scss';

Vue.config.productionTip = false;

moment.locale('nl');

// // Install BootstrapVue
// Vue.use(BootstrapVue);
// // Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin);

const vue = new Vue({
  render: h => h(App),
});
vue.$mount('#app');
