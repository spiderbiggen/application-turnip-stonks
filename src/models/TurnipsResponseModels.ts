export type TokenResponse = {
  access_token: string;
  expires_in: string;
  refresh_token?: string;
  refresh_token_expires_in?: string;
}

export type Player = {
  username: string;
  island: string;
}

export type FullPlayer = Player & {
  id?: string;
  username: string;
  island: string;
  socketId?: string,
  invited?: boolean,
  hosting?: boolean,
  sessionId?: string,
}

export type RoomUpdate = { room_id: string, max_users: number, current_users: number, accepted: Player[], island: Player, message?: string | null };
