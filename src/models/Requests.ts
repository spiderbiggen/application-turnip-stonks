export interface TokenRequest {
  token: string,
  refresh_token?: string,
}
