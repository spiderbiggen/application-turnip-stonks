
export interface KeyedEntity {
  id: string;
}

export interface DatedEntity extends KeyedEntity {
  created_at: Date;
  updated_at: Date;
}
